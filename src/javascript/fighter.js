class Fighter {
  constructor(name, health, attack, defense) {
    this.name = name;
    this.health = health;
    this.attack = attack;
    this.defense = defense;
  }

  // сила удара (количество нанесенного урона здоровью противника)
  getHitPower() {
    const criticalHitChance = Math.floor(Math.random() * 2) + 1; // число от 1 до 2
    const power = this.attack * criticalHitChance;
    return power;
  }

  // сила блока (амортизация удара противника)
  getBlockPower() {
    const dodgeChance = Math.floor(Math.random() * 2) + 1;
    const power = this.defense * dodgeChance;
    return power;
  }
}

export default Fighter;
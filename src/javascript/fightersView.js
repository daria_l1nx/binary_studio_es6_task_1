import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import { modalWindow } from './helpers/modalWindow';
import Arena from './arena';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();
  arena = new Arena();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    const fightBtn = document.createElement('a');
    fightBtn.innerText = 'Start fight';
    fightBtn.className = 'fight-btn';
    fightBtn.onclick = () => this.arena.isReadyToFight();

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
    this.element.appendChild(fightBtn);
  }

  async handleFighterClick(event, fighter) {
    this.fightersDetailsMap.set(fighter._id, fighter);
    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal

    try {
      const fighterDetails = await fighterService.getFighterDetails(fighter._id)
        .then(data => {
          this.fightersDetailsMap.set(fighter._id, data);

          const selectedEl = document.getElementById(fighter.name);
          selectedEl.toggleAttribute('checked');

          const selectedFighter = this.fightersDetailsMap.get(fighter._id, fighter);
          modalWindow(selectedFighter);
        
          this.arena.setFighter(selectedFighter);
        })
    } catch (error) {
      console.error(error);
    }
  }
}

export default FightersView;
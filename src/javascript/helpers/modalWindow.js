function modalWindow(data) {
  const rootElement = document.getElementById('root');
  const modal = document.createElement('div');
  const modalContent = document.createElement('div');
  const closeEl = document.createElement('a');

  closeEl.innerText = 'Close';
  closeEl.onclick = () => {
    rootElement.removeChild(modal);
  }

  modalContent.className = 'modal-content';
  modalContent.innerHTML = `
    <h4>${data.name}</h4>
    <p>
      <label for="health">Health</label>
      <input type="number" name="health" value=${data.health}>
    </p>
    <p>
      <label for="attack">Attack</label>
      <input type="number" name="attack" value=${data.attack}>
    </p>
    <p>
      <label for="defense">Defense</label>
      <input type="number" name="defense" value=${data.defense}>
    </p>
  `;

  modalContent.appendChild(closeEl);

  modal.className = 'modal';
  modal.appendChild(modalContent);

  rootElement.appendChild(modal);
}

export { modalWindow };
import Fighter from './fighter';

class Arena {
  fighters = [];

  setFighter(fighter) {
    this.fighters.push(fighter);
  }

  isReadyToFight() {
    if (this.fighters.length >= 2) {
      this.fight(this.fighters);
    }
  }

  fight(fighters) {    
    const fighter1 = new Fighter(fighters[0].name, fighters[0].health, fighters[0].attack, fighters[0].defense);
    const fighter2 = new Fighter(fighters[1].name, fighters[1].health, fighters[1].attack, fighters[1].defense);

    console.log(`Начало боя. ${fighter1.name}(${fighter1.health}) vs ${fighter2.name}(${fighter2.health})`);

    while (fighter1.health >= 0 || fighter2.health >= 0) {
      const f1Hit = fighter1.getHitPower();
      const f2Block = fighter2.getBlockPower();

      console.log(`${fighter1.name}(${fighter1.health}) наносит ${f1Hit} урона ${fighter2.name}(${fighter2.health}) блокирует ${f2Block} урона`);

      fighter2.health = fighter2.health - Math.abs(f1Hit - f2Block);

      if (fighter1.health <= 0) {
        console.log(`${fighter2.name} победил`);
        break;
      } else if (fighter2.health <= 0) {
        console.log(`${fighter1.name} победил`);
        break;
      }

      const f2Hit = fighter2.getHitPower();
      const f1Block = fighter1.getBlockPower();

      console.log(`${fighter2.name}(${fighter2.health}) наносит ${f2Hit} урона ${fighter1.name}(${fighter1.health}) блокирует ${f1Block} урона`);

      fighter1.health = fighter1.health - Math.abs(f2Hit - f1Block);

      if (fighter1.health <= 0) {
        console.log(`${fighter2.name} победил`);
        break;
      } else if (fighter2.health <= 0) {
        console.log(`${fighter1.name} победил`);
        break;
      }
    }
  }
}

export default Arena;